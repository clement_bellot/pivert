﻿using System;

namespace Pivert.Persistence.Git
{
    [Serializable]
    public class MalformatedRepositoryException : Exception
    {
        public MalformatedRepositoryException(string message) : base(message) { }
        protected MalformatedRepositoryException(
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context)
            : base(info, context) { }
    }
}
