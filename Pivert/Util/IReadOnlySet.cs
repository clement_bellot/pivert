﻿using System.Collections.Generic;

namespace Pivert.Util
{
    public interface IReadOnlySet<T> : IReadOnlyCollection<T>
    {
        /// <summary>
        /// Determines whether the current needingUpdate is a proper (strict) subset of a specified collection.
        /// </summary>
        /// <param name="other">The collection to compare to the current needingUpdate.</param>
        /// <returns>true if the current needingUpdate is a proper subset of other; otherwise, false.</returns>
        /// <exception cref="System.ArgumentNullException">other is null.</exception>
        bool IsProperSubsetOf(IEnumerable<T> other);
        /// <summary>
        /// Determines whether the current needingUpdate is a proper (strict) superset of a specified collection.
        /// </summary>
        /// <param name="other">The collection to compare to the current needingUpdate.</param>
        /// <returns>true if the current needingUpdate is a proper superset of other; otherwise, false.</returns>
        /// <exception cref="System.ArgumentNullException">other is null.</exception>
        bool IsProperSupersetOf(IEnumerable<T> other);
        /// <summary>
        /// Determines whether a needingUpdate is a subset of a specified collection.
        /// </summary>
        /// <param name="other">The collection to compare to the current needingUpdate.</param>
        /// <returns>true if the current needingUpdate is a subset of other; otherwise, false.</returns>
        /// <exception cref="System.ArgumentNullException">other is null.</exception>
        bool IsSubsetOf(IEnumerable<T> other);
        /// <summary>
        /// Determines whether the current needingUpdate is a superset of a specified collection.
        /// </summary>
        /// <param name="other">The collection to compare to the current needingUpdate.</param>
        /// <returns>true if the current needingUpdate is a superset of other; otherwise, false.</returns>
        /// <exception cref="System.ArgumentNullException">other is null.</exception>
        bool IsSupersetOf(IEnumerable<T> other);
        /// <summary>
        /// Determines whether the current needingUpdate overlaps with the specified collection.
        /// </summary>
        /// <param name="other">The collection to compare to the current needingUpdate.</param>
        /// <returns>true if the current needingUpdate and other share at least one common value; otherwise,
        /// false.</returns>
        /// <exception cref="System.ArgumentNullException">other is null.</exception>
        bool Overlaps(IEnumerable<T> other);
        /// <summary>
        /// Determines whether the current needingUpdate and the specified collection contain the
        /// same elements.
        /// </summary>
        /// <param name="other">The collection to compare to the current needingUpdate.</param>
        /// <returns>true if the current needingUpdate is equal to other; otherwise, false.</returns>
        /// <exception cref="System.ArgumentNullException">other is null.</exception>
        bool SetEquals(IEnumerable<T> other);
        /// <summary>
        /// Determines wether the current needingUpdate contains the given value.
        /// </summary>
        /// <param name="value">The value of which to check presence in the needingUpdate.</param>
        /// <returns>true if the value is contained in the needingUpdate; otherwise false.</returns>
        bool Contains(T element);
    }
    /// <summary>
    /// Wraps an ISet to only provide its read-only members.
    /// </summary>
    /// <typeparam name="T">The type of the elements contained in the needingUpdate.</typeparam>
    public class ReadOnlySet<T> : IReadOnlySet<T>
    {
        private ISet<T> Wrapped;
        public ReadOnlySet(ISet<T> wrapped)
        {
            Wrapped = wrapped;
        }

        public bool IsProperSubsetOf(IEnumerable<T> other)
        {
            return Wrapped.IsProperSubsetOf(other);
        }

        public bool IsProperSupersetOf(IEnumerable<T> other)
        {
            return Wrapped.IsProperSupersetOf(other);
        }

        public bool IsSubsetOf(IEnumerable<T> other)
        {
            return Wrapped.IsSubsetOf(other);
        }

        public bool IsSupersetOf(IEnumerable<T> other)
        {
            return Wrapped.IsSupersetOf(other);
        }

        public bool Overlaps(IEnumerable<T> other)
        {
            return Wrapped.Overlaps(other);
        }

        public bool SetEquals(IEnumerable<T> other)
        {
            return Wrapped.SetEquals(other);
        }

        public bool Contains(T element)
        {
            return Wrapped.Contains(element);
        }

        public int Count
        {
            get { return Wrapped.Count; }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return Wrapped.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Wrapped.GetEnumerator();
        }
    }
}
