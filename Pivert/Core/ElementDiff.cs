﻿namespace Pivert.Core
{
#if FALSE
    public class ElementDiff
    {
        public Table Original { get; private set; }
        public Table Modified { get; private set; }
        /// <summary>
        /// Whether the two elements are identical when taken property per property.
        /// </summary>
        public bool Identical
        {
            get
            {
                return ModifiedKeys.Count == 0
                    && ChangedIndices.Count == 0
                    && !TargetChanged
                    && !ValueChanged
                    && SameElementId;
            }
        }
        public IReadOnlyList<string> ModifiedKeys { get { return _modifiedKeys; } }
        /// <summary>
        /// If the diffed elements are List s contains the list of the indexes that does not
        /// reference the same element.
        /// </summary>
        public IReadOnlyList<int> ChangedIndices { get { return _changedIndices; } }
        /// <summary>
        /// Whether the diffed elements are SymbolRef s and are referencing the same element.
        /// </summary>
        public bool TargetChanged { get; private set; }
        /// <summary>
        /// Whether the diffed elements are Value s, and their value is not the same.
        /// </summary>
        public bool ValueChanged { get; private set; }
        /// <summary>
        /// Whether the diffed elements have the same identifier, i.e. whether they were
        /// the same element at one point.
        /// </summary>
        public bool SameElementId { get { return Original.ID == Modified.ID; } }
        /// <summary>
        /// Whether the 
        /// </summary>
        public bool DifferentBaseType { get; private set; }

        private List<string> _modifiedKeys = new List<string>();
        private List<int> _changedIndices = new List<int>();

        public ElementDiff(Table original, Table modified, bool throwIfDifferentElements = true)
        {
            Original = original;
            Modified = modified;
            if (original == null) { throw new ArgumentNullException("original"); }
            if (modified == null) { throw new ArgumentNullException("modified"); }
            if (throwIfDifferentElements && !SameElementId)
            {
                throw new ArgumentException("original and modified must have the same id "
                        + "when throwIfDifferentElements is true.");
            }
            MetadataDiff();
            DifferentBaseType = false;
            ValueChanged = false;
            TargetChanged = false;
            Construct((dynamic)original, (dynamic)modified);
            if (DifferentBaseType && SameElementId)
            {
                throw new ArgumentException(
                    "Two elements with the same id cannot have the a different base type");
            }
        }
        private void Construct(List original, List modified)
        {
            int i = 0;
            for (i = 0; i < Math.Min(original.Count, modified.Count); ++i)
            {
                if (original[i].Id != modified[i].Id)
                {
                    _changedIndices.Add(i);
                }
            }
            for (; i < Math.Max(original.Count, modified.Count); ++i)
            {
                _changedIndices.Add(i);
            }
        }
        private void Construct(Value original, Value modified)
        {
            ValueChanged = !original.Content.Equals(modified.Content);
        }
        private void Construct(Reference original, Reference modified)
        {
            TargetChanged = !original.Referenced.Equals(modified.Referenced);
        }
        private void Construct(Undefined original, Undefined modified)
        {
            // Do nothing.
        }
        private void Construct(Table original, Table modified)
        {
            DifferentBaseType = true;
        }
        private bool AssertSameType<T>(Table e1, Table e2)
        {
            if (e1 is T && !(e2 is T))
            {
                throw new ArgumentException("original and modified must be of the same base type "
                        + "when throwIfDifferentElements is true.");
            }
            return e2 is T;
        }
    }
#endif
}
