﻿using System.Collections.Generic;

namespace Pivert.Core
{
    public interface IVersionnedCode
    {
        IList<string> Branches { get; }
        string LatestState { get; }
        string CurrentBranchId { get; }
        /// <summary>
        /// The last changeset that was created in this branch.
        /// </summary>
        string LatestChangeSetID { get; }
        /// <summary>
        /// The changeset that will be used when <c>UpdateElement</c> or
        /// <c>GetElement</c> is used.
        /// </summary>
        string CurrentChangeSetID { get; }

        /// <summary>
        /// Sets the changeset to use for <c>UpdateElement</c> and <c>GetElement</c>.
        /// </summary>
        /// <param name="targetChangeset">The new current changeset.</param>
        void ToChangeSet(string targetChangeset);

        /// <summary>
        /// Creates a branch starting from the current changeset.
        /// </summary>
        /// <returns>The new branch identifier.</returns>
        string Branch();
    }
}
