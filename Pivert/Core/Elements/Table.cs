﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Runtime.Serialization;

namespace Pivert.Core
{
    /// <summary>
    /// An Table maps string to values.
    /// 
    /// Those values can be either an Table, a reference to an Table, a list of Table, or a string.
    /// 
    /// It is more accurately descibed by "Object", but it would have forced
    /// explicit namespace specification for each use (colliding with C# Object).
    /// </summary>
    public sealed class Table : ElementContainer, IDictionary<string, Element>
    {
        private Dictionary<string, Element> _contents = null;
        private Dictionary<string, Element> Contents
        {
            get
            {
                if (_contents == null)
                {
                    _contents = CodeFromWhichToLoad.LoadTableContents(this);
                }
                return _contents;
            }
            set { _contents = value; }
        }

        public Table()
        {
            _contents = new Dictionary<string, Element>();
        }

        /// <summary>
        /// Loads an value from an external source.
        /// </summary>
        /// <param name="c"></param>
        /// <param name="metadata"></param>
        public Table(IPersistentCode c, ElementContainer parent)
            : base(c, parent) { }

        public override dynamic ToDynamic()
        {
            return new DynamicObjectAccess(this);
        }
        public override Element Copied()
        {
            var ret = new Table();
            foreach (var kv in this.Contents)
            {
                ret._contents[kv.Key] = kv.Value.Copied();
            }
            return ret;
        }
        #region IDictionary implementation
        public Element this[string key]
        {
            get { return Contents[key]; }
            set
            {
                if (key == null) { throw new ArgumentNullException(); }
                if (value == null)
                {
                    if (Contents.ContainsKey(key))
                    {
                        OnChanged();
                    }
                    Contents.Remove(key);
                    return;
                }
                EnsureNoLoops(value);
                Contents[key] = value;
                value.Parent = this;
                OnChanged();
            }
        }
        public void Add(string key, Element value)
        {
            this[key] = value;
        }
        public bool ContainsKey(string key)
        {
            return Contents.ContainsKey(key);
        }
        public ICollection<string> Keys
        {
            get { return Contents.Keys; }
        }
        public bool Remove(string key)
        {
            if (Contents.Remove(key))
            {
                OnChanged();
                return true;
            }
            return false;
        }
        public bool TryGetValue(string key, out Element value)
        {
            return Contents.TryGetValue(key, out value);
        }
        #region IDictionary boilerplate.
        ICollection<Element> IDictionary<string, Element>.Values
        {
            get { return Contents.Values; }
        }

        /*Element IDictionary<string, Element>.this[string key]
        {
            get
            {
                throw new NotImplementedException();
            }
            needingUpdate
            {
                throw new NotImplementedException();
            }
        }*/
        void ICollection<KeyValuePair<string, Element>>.Add(KeyValuePair<string, Element> item)
        {
            this[item.Key] = item.Value;
        }
        void ICollection<KeyValuePair<string, Element>>.Clear()
        {
            Contents.Clear();
            OnChanged();
        }
        bool ICollection<KeyValuePair<string, Element>>.Contains(KeyValuePair<string, Element> item)
        {
            return Contents.Contains(item);
        }
        void ICollection<KeyValuePair<string, Element>>.CopyTo(KeyValuePair<string, Element>[] array, int arrayIndex)
        {
            ((IDictionary<string, Element>)Contents).CopyTo(array, arrayIndex);
        }
        public override int Count
        {
            get { return Contents.Count; }
        }
        bool ICollection<KeyValuePair<string, Element>>.IsReadOnly
        {
            get { return false; }
        }
        bool ICollection<KeyValuePair<string, Element>>.Remove(KeyValuePair<string, Element> item)
        {
            Element actualValue;
            if (Contents.TryGetValue(item.Key, out actualValue) && actualValue == item.Value)
            {
                Contents.Remove(item.Key);
                return true;
            }
            return false;
        }
        IEnumerator<KeyValuePair<string, Element>> IEnumerable<KeyValuePair<string, Element>>.GetEnumerator()
        {
            return Contents.GetEnumerator();
        }
        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Contents.GetEnumerator();
        }
        #endregion
        #endregion
        #region ElementContainer implementation
        public override IEnumerator<Element> GetEnumerator()
        {
            foreach (var entry in this as IEnumerable<KeyValuePair<string, Element>>)
            {
                yield return entry.Value;
            }
        }
        public override bool Contains(Element item)
        {
            foreach (var element in this)
            {
                if (element.Equals(item))
                {
                    return true;
                }
            }
            return false;
        }
        public override bool Remove(Element item)
        {
            foreach (var entry in this as IEnumerable<KeyValuePair<string, Element>>)
            {
                if (entry.Value.Equals(item))
                {
                    this[entry.Key] = null;
                    return true;
                }
            }
            return false;
        }
        internal override bool ContentIsLoaded
        {
            get { return _contents != null; }
        }
        #endregion

    }
    /// <summary>
    /// Wraps an <c>Table</c> so that it can be accessed as a <c>dynamic</c>.
    /// </summary>
    public class DynamicObjectAccess : DynamicElementAccess<Table>
    {
        public DynamicObjectAccess(Table wrapped)
            : base(wrapped) { }
        public override bool TryGetMember(GetMemberBinder binder, out object result)
        {
            result = _wrapped[binder.Name].ToDynamic();
            return true;
        }
        public override bool TrySetMember(SetMemberBinder binder, object value)
        {
            if (value == null)
            {
                _wrapped.Remove(binder.Name);
            }
            else
            {
                _wrapped[binder.Name] = DecryptElementAssignable(value);
            }
            return true;
        }
        public override IEnumerable<string> GetDynamicMemberNames()
        {
            return _wrapped.Keys;
        }
        public override bool TryDeleteMember(DeleteMemberBinder binder)
        {
            _wrapped.Remove(binder.Name);
            return true;
        }
    }
    [Serializable]
    public class InvalidKeyException : Exception
    {
        public InvalidKeyException(string key)
        {
            Data["key"] = key;
        }
        public override string Message
        {
            get
            {
                return "Keys starting with an underscore are reserved for implementation.";
            }
        }
        protected InvalidKeyException(
          SerializationInfo info,
          StreamingContext context)
            : base(info, context) { }
    }
}
