﻿namespace Pivert.Core
{
    public sealed class Undefined : Element
    {
        public Undefined() { }
        public Undefined(IPersistentCode code,ElementContainer parent)
            : base(code, parent) { }
        public override dynamic ToDynamic()
        {
            return null;
        }
        public override Element Copied()
        {
            return new Undefined();
        }
        internal override bool ContentIsLoaded
        {
            get { return true; }
        }
    }
}
