﻿using System;
using System.Collections.Generic;

namespace Pivert.Core
{
    public abstract class Element
    {
        private void AssumeValidSubtype()
        {
            if (!(this is Value || this is Undefined || this is Table || this is List))
            {
                throw new InvalidSubTypeException(this.GetType().FullName);
            }
        }
        public Element() { AssumeValidSubtype(); }
        public Element(IPersistentCode c, ElementContainer parent)
        {
            AssumeValidSubtype();
            CodeFromWhichToLoad = c;
            Parent = parent;
        }
        private ElementContainer _parent;
        public ElementContainer Parent
        {
            get { return _parent; }
            internal set
            {
                if (_parent != null)
                {
                    RemoveParent();
                }
                _parent = value;
            }
        }
        public void RemoveParent()
        {
            Parent.Remove(this);
            _parent = null;
        }
        public readonly IPersistentCode CodeFromWhichToLoad;
        private IPersistentCode _codeToNotifyOfChanges;
        public IPersistentCode CodeToNotifyOfChanges
        {
            get {
                if (Parent == null)
                {
                    return _codeToNotifyOfChanges;
                }
                return Parent.CodeToNotifyOfChanges;
            }
        }
        /// <summary>
        /// Should be used by the IPersistentCode when a new element is assigned to its TopLevel property.
        /// </summary>
        /// <param name="newCode">The IPersistentCode to which the element was added as the TopLevel.</param>
        public void SetCodeForTopLevel(IPersistentCode newCode)
        {
            if (Parent != null)
            {
                throw new InvalidOperationException("Cannot call this method on an element with a parent.");
            }
            _codeToNotifyOfChanges = newCode;
            var needingUpdate = new HashSet<Element>();
            needingUpdate.Add(this);
            foreach (var elementNeedingUpdate in needingUpdate)
            {
                elementNeedingUpdate.OnChanged();
            }
        }
        /// <summary>
        /// Returns true if e1 is a parent of e2, or if e1 is e2.
        /// </summary>
        /// <param name="e1"></param>
        /// <param name="e2"></param>
        /// <returns></returns>
        protected static bool IsParentOrSame(ElementContainer e1, Element e2)
        {
            if (e1 == null || e2 == null) { return false; }
            return e1 == e2 || IsParentOrSame(e1, e2.Parent);
        }
        public abstract dynamic ToDynamic();
        /// <summary>
        /// Deeply copies the element structure. The code is needingUpdate to null.
        /// The parent is needingUpdate to null.
        /// </summary>
        /// <returns></returns>
        public abstract Element Copied();
        internal abstract bool ContentIsLoaded { get; }

        protected void OnChanged()
        {
            if (CodeToNotifyOfChanges != null)
            {
                CodeToNotifyOfChanges.RegisterChangedElement(this);
            }
        }
    }
    [Serializable]
    public class InvalidSubTypeException : Exception
    {
        public InvalidSubTypeException(string subTypeName)
            : base("Element cannot be subtyped outside of Pivert.Core.")
        {
            Data["type"] = subTypeName;
        }
    }
}
