﻿using System;
using System.Collections.Generic;

namespace Pivert.Core
{
    public abstract class ElementContainer : Element, ICollection<Element>
    {
        public ElementContainer() { }
        public ElementContainer(IPersistentCode c, ElementContainer parent)
            : base(c, parent) { }
        void ICollection<Element>.Add(Element item)
        {
            throw new NotSupportedException();
        }

        void ICollection<Element>.Clear()
        {
            throw new NotSupportedException();
        }

        protected void EnsureNoLoops(Element elementAdded)
        {
            if (IsParentOrSame(elementAdded as ElementContainer, this))
            {
                throw new ArgumentException("A loop has been created."
                    + " An element cannot contain one of its parents.");
            }
        }

        public abstract bool Contains(Element item);

        public void CopyTo(Element[] array, int arrayIndex)
        {
            if (array == null || arrayIndex + Count >= array.Length)
            {
                throw new ArgumentException("array");
            }
            if (arrayIndex < 0)
            {
                throw new ArgumentOutOfRangeException("arrayIndex");
            }
            int currentPosition = arrayIndex;
            foreach (Element e in this)
            {
                array[currentPosition] = e;
                ++currentPosition;
            }
        }

        public abstract int Count { get; }

        public bool IsReadOnly
        {
            get { return false; }
        }

        public abstract bool Remove(Element item);

        public abstract IEnumerator<Element> GetEnumerator();

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
