﻿using System;
using System.Collections.Generic;

namespace Pivert.Core
{
    /// <summary>
    /// This represents a linked list.
    /// Setting an index to null will call removeAt
    /// </summary>
    public sealed class List : ElementContainer, IList<Element>
    {
        private List<Element> _contents;
        private List<Element> Contents
        {
            get
            {
                if (!ContentIsLoaded)
                {
                    _contents = CodeFromWhichToLoad.LoadListContents(this);
                }
                return _contents;
            }
            set
            {
                _contents = value;
            }
        }
        public List()
        {
            _contents = new List<Element>();
        }
        public List(IEnumerable<Element> from)
            : this()
        {
            _contents = new List<Element>();
            foreach (var o in from)
            {
                Add(o);
            }
        }
        public List(IPersistentCode c, ElementContainer parent)
            : base(c, parent) { }

        public override dynamic ToDynamic()
        {
            return new DynamicListAccess(this);
        }
        public override Element Copied()
        {
            var ret = new List();
            foreach (var element in this.Contents)
            {
                ret.Contents.Add(element.Copied());
            }
            return ret;
        }

        #region IList implementation
        public void Insert(int index, Element item)
        {
            if (index < 0 || index > Contents.Count) { throw new IndexOutOfRangeException(); }
            if (item == null) { throw new ArgumentNullException("item"); }
            EnsureNoLoops(item);
            Contents.Insert(index, item);
            item.Parent = this;
            OnChanged();
        }

        public void RemoveAt(int index)
        {
            if (index < 0 || index >= Contents.Count) { throw new IndexOutOfRangeException(); }
            Contents[index].Parent = null;
            Contents.RemoveAt(index);
            OnChanged();
        }
        #region IList boilerplate
        public int IndexOf(Element item)
        {
            return Contents.IndexOf(item);
        }
        public Element this[int index]
        {
            get
            {
                return Contents[index];
            }
            set
            {
                RemoveAt(index);
                Insert(index, value ?? new Undefined());
            }
        }

        public void Add(Element item)
        {
            Insert(Count, item);
        }

        public void Clear()
        {
            while (Count > 0)
            {
                RemoveAt(0);
            }
        }

        public override bool Contains(Element item)
        {
            return Contents.Contains(item);
        }

        public new void CopyTo(Element[] array, int arrayIndex)
        {
            Contents.CopyTo(array, arrayIndex);
        }

        public override int Count
        {
            get { return Contents.Count; }
        }

        public override bool Remove(Element item)
        {
            int idx = IndexOf(item);
            if (idx == -1)
            {
                return false;
            }
            RemoveAt(idx);
            return true;
        }

        public override IEnumerator<Element> GetEnumerator()
        {
            return Contents.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return Contents.GetEnumerator();
        }
        #endregion
        #endregion

        internal override bool ContentIsLoaded
        {
            get { return _contents != null; }
        }

    }
    public class DynamicListAccess : DynamicElementAccess<List>
    {
        public DynamicListAccess(List wrapped)
            : base(wrapped) { }
        private int? getIndex(object[] indexes)
        {
            return indexes.Length == 1 ? indexes[0] as int? : null;
        }
        public override bool TryGetIndex(System.Dynamic.GetIndexBinder binder, object[] indexes, out object result)
        {
            var idx = getIndex(indexes);
            if (indexes.Length != 1 || idx < 0 || idx >= _wrapped.Count)
            {
                result = null;
                return false;
            }
            result = _wrapped[(int)idx].ToDynamic();
            return true;
        }
        public override bool TryDeleteIndex(System.Dynamic.DeleteIndexBinder binder, object[] indexes)
        {
            _wrapped.RemoveAt((int)getIndex(indexes));
            return true;
        }
        public override bool TrySetIndex(System.Dynamic.SetIndexBinder binder, object[] indexes, object value)
        {
            _wrapped[(int)getIndex(indexes)] = DecryptElementAssignable(value);
            return true;
        }
        public void RemoveAt(int idx)
        {
            _wrapped.RemoveAt(idx);
        }
        public void Add(dynamic value)
        {
            _wrapped.Add(DecryptElementAssignable(value));
        }
        public void Insert(int idx, dynamic value)
        {
            _wrapped.Insert(idx, DecryptElementAssignable(value));
        }
    }
}
