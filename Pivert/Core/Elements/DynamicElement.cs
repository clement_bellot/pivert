﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Dynamic;
namespace Pivert.Core
{
    interface IDynamicElement<out TWrapped>
     where TWrapped : Element
    {
        dynamic _parent { get; }
        string _type { get; }
        TWrapped _wrapped { get; }
    }

    public abstract class DynamicElementAccess<TWrapped> : DynamicObject, IDynamicElement<TWrapped> where TWrapped : Element
    {
        protected DynamicElementAccess(TWrapped wrapped)
        {
            _wrapped = wrapped;
        }
        public TWrapped _wrapped { get; private set; }
        public string _type { get { return _wrapped.GetType().Name.ToLower(); } }
        public dynamic _parent
        {
            get
            {
                if (_wrapped.Parent == null)
                {
                    return null;
                }
                else
                {
                    return _wrapped.Parent.ToDynamic();
                }
            }
        }
        protected static bool TryDecryptElement(dynamic element, out Element result)
        {
            if (element is Element)
            {
                result = element;
                return true;
            }
            else if (element is IDynamicElement<Element>)
            {
                result = element._wrapped;
                return true;
            }
            else
            {
                result = null;
                return false;
            }
        }
        protected static Element DecryptElement(dynamic element)
        {
            Element result;
            if (TryDecryptElement(element, out result))
            {
                return result;
            }
            else
            {
                throw new ArgumentException("Bad type. Expected DynamicElement or Element.");
            }
        }
        protected static Element DecryptElementAssignable(dynamic value)
        {
            Element result;
            if (TryDecryptElement(value, out result))
            {
                return result;
            }
            else if (value is string)
            {
                return new Value(value as string);
            }
            else if (value is IDictionary<string,object>)
            {
                var ret = new Table();
                foreach(var kv in value as IDictionary<string,object>){
                    ret[kv.Key] = DecryptElementAssignable(kv.Value);
                }
                return ret;
            }
            else if (value is IEnumerable<object>)
            {
                return new List(
                    from o in (value as IEnumerable<object>)
                    select DecryptElementAssignable(o));
            }
            else
            {
                throw new ArgumentException("Bad type. Expected DynamicElement or Element or string.");
            }
        }
    }
}
