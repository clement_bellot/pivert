﻿using System;

namespace Pivert.Core
{
    public sealed class Value : Element
    {
        public Value(string value)
        {
            if (value == null) { throw new ArgumentNullException(); }
            _content = value;
        }
        public Value(IPersistentCode c, ElementContainer parent)
            : base(c, parent)
        {
        }
        public static implicit operator string(Value @this)
        {
            return @this.Content;
        }
        internal override bool ContentIsLoaded
        {
            get { return _content != null; }
        }
        private string _content;
        public string Content
        {
            get
            {
                if (!ContentIsLoaded)
                {
                    _content = CodeFromWhichToLoad.LoadValueContents(this);
                }
                return _content;
            }
            internal set
            {
                if (value == null) { throw new ArgumentNullException(); }
                _content = value;
                OnChanged();
            }
        }
        public override dynamic ToDynamic()
        {
            return new DynamicValueAccess(this);
        }
        public override Element Copied()
        {
            return new Value(Content);
        }
    }
    public class DynamicValueAccess : DynamicElementAccess<Value>
    {
        public DynamicValueAccess(Value v)
            :base(v)
        {
        }
        public static implicit operator string(DynamicValueAccess @this)
        {
            return @this._wrapped.Content;
        }
    }
}
