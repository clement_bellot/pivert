﻿using System.Collections.Generic;

namespace Pivert.Core
{
    /// <summary>
    /// Represents a "bunch" of code. It should correspond to one library (DLL) or executable.
    /// 
    /// For a given ElementId, should always return the same Element instance.
    /// </summary>
    public interface IPersistentCode
    {
        void RegisterChangedElement(Element e);

        Dictionary<string, Element> LoadTableContents(Table element);
        List<Element> LoadListContents(List element);
        string LoadValueContents(Value element);
        Element TopLevel { get; set; }

        /// <summary>
        /// Commits the current changes to the database.
        /// </summary>
        /// <param name="data">Data associated with the commit.</param>
        void Commit(string data);
    }
}
