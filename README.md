# About #

This project provides source control for json-like data. Currently, only a git backend is implemented.

Supported elements are:

* Table (a dictionary of string to element)

* List (a ordered list of elements

* Value (a string)

* Undefined (intended to be used as a placeholder).

The elements are lazily loaded, can be modified in an imperative way, moved from a code to another, and committed.

# Status #

Basic functionality works, use at your own risk.

# Known bugs #

The git backend will fail on Windows if Table keys contains some characters or are too long (still haven't gotten to fix my pull request).